# Introduction

A web application that can validate iFDOs against a version of the standard. Requires the ontology server.

### TODO
- Use Docker container, flask, Python
- build web page with option to load iFDO via URL or upload an iFDO file
- validate the iFDO file against one (all) iFDO standard versions provided by the ontology app
- create and show human-readale report (badge?) 
- provide machine-readable endpoint
